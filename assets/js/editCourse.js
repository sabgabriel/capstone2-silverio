let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
//console.log(courseId)
let token = localStorage.getItem("token");


fetch(`https://infinite-brook-06577.herokuapp.com/api/course/${courseId}`)
.then (res=> res.json())
.then(data => {


	document.querySelector('#courseName').placeholder = data.name
	document.querySelector('#coursePrice').placeholder = data.price
	document.querySelector('#courseDescription').placeholder = data.description

})

let editForm = document.querySelector("#editCourse")

editForm.addEventListener("submit", (e)=>{
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	fetch('https://infinite-brook-06577.herokuapp.com/api/course', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			id: courseId,
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res=> res.json())
	.then(data=>{
		if (data === true){
			alert("Course was edited successfuly")
			window.location.replace('./courses.html')
		}else{
			alert("Something went wrong.")
		}
	})
})