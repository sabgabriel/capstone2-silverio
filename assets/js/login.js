let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) => {

	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	console.log(email);
	console.log(password);

	if (email == "" || password == "") {
		alert("Please input your email and/or password.")
	} else {

		fetch('https://infinite-brook-06577.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {


			if (data.accessToken) {

				//Set the token into the local storage
				localStorage.setItem('token', data.accessToken)

				//Send a fetch request to decode the JWT and obtain the user isAdmin property
				fetch('https://infinite-brook-06577.herokuapp.com/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}


				})
				.then(res => res.json())
				.then(data => {
					localStorage.setItem('userId', data._id)
					localStorage.setItem('isAdmin', data.isAdmin)

					//window.location.replace = this allows to redirect our user to another page
					window.location.replace("./courses.html")
				})

			} else {

				alert("Login failed. Something went wrong.")



			}
			
		})

	}

})
