let registerForm = document.querySelector("#registerUser")

//addEventListener("event", function)
/*
	Allows us to add a specific event into an element. This event can trigger a function for our page to do.
*/

//Submit event - allows us to submit a form.
//It's default behavior is that it sends your form and refreshes the page
registerForm.addEventListener("submit", (e) => {

	//e = event object. This event object pertains to the event and where it was triggered.
	//preventDefault() = prevents the submit event of its default behavior

	e.preventDefault()

	//console.log("I triggered the submit event")

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value


	if((password1 !== '' && password2 !== '' ) && (password1 === password2) && (mobileNo.length === 11)){

		//fetch (url, options)
		fetch('https://infinite-brook-06577.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json'}, //meaning content to be passed is in JSON
			body: JSON.stringify({ //pass to server in string, then convert to .json when server pass back to backend
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			//If another FETCH, should always be next the .THEN (data)
			//True = email exist, false = email doesnt exist
			console.log(data)

		//REGISTER USER
			if (data === false) {
				// (/users only because route is only
				/* router.post('/', (req, res) => {
				userController.register(req.body).then(result => res.send(result))
				})*/
				fetch('https://infinite-brook-06577.herokuapp.com/api/users', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json'},
				body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);
					//here have ; because of end of line of code, line 41 doesnt have because continual execution of code
					if (data === true){
                		alert("Registered successfully");
                		window.location.replace("login.html");
                	} else {
                		alert("Something went wrong");
                	}
                })
    		} else {
    			alert("Email has been used. Try a different one.")
    		}
    	})

    } else {
    	alert("Something went wrong")
    }
})