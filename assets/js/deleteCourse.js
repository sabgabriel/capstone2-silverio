let params = new URLSearchParams(window.location.search)
let jwtToken = localStorage.getItem('token')
let courseId = params.get('courseId')

fetch(`https://infinite-brook-06577.herokuapp.com/api/course/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Authorization': `Bearer ${jwtToken}`
	}
})
.then(res => res.json())
.then(data =>{
	//console.log(data)
	if (data === true){
		window.location.replace('./courses.html')
	}else{
		alert("Something went wrong.")
	}
})