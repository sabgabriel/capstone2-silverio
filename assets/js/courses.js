//retrieve the user information for isAdmin
let adminUser = localStorage.getItem("isAdmin");
//console.log(adminUser);
let token = localStorage.getItem('token');
//add
let coursesContainer = document.querySelector("#noCourse")
//for storage of edit, archive and enable buttons
let cardFooter; 


fetch('https://infinite-brook-06577.herokuapp.com/api/course', {
 method: 'GET',
 headers: {
 	'Authorization': `Bearer ${token}`
 }
})
.then(res => res.json())
.then(data => {
// console.log(data);
//console.log(token);
if(token == null){
  alert('Please login or sign up to enroll in a course')
  window.location.replace('register.html');
  
}
    //store message if no courses available
	//let courseData;

		// if(data.length < 1) {
		// courseData = "No courses available"
		// } else {

    if (data.length < 1) {

        coursesContainer.innerHTML = 
        `
        <h5 class='text-center'>Sorry, there are no courses available at the moment.</h5>
        `

    } else {
		courseData = data.map(course => { //return array

			//console.log(course._id);

			
			//select course button only
			if(!adminUser || adminUser === "false") {
				cardFooter =
				`
                 <a href="./course.html?courseId=${course._id}" value=${course._id} id="selectBtn" class="btn btn-info text-white btn-block editButton"> Select Course </a>

				`
              
			} else { //admin buttons
                cardFooter = 
                `
                
                <a href="./course.html?courseId=${course._id}" value=${course._id}  id="viewButton" class="btn btn-secondary text-white btn-block secondaryButton">View Enrollees </a>
                <a href="./editCourse.html?courseId=${course._id}" value=${course._id} id="editButton" class="btn btn-info text-white btn-block editButton"> Edit </a>
                `

                //active course
                let isActive = course.isActive;

                if(!isActive){
                    //console.log(isActive)
                    cardFooter = cardFooter +
                    ` 
                    <button id= '${course._id}' value= 1 onclick="enableDisable('${course._id}', this.value)"  id= "enableButton" class="btn btn-success text-white btn-block dangerButton"> Enable Course </button> 
                    
                    `

                } else {
                    cardFooter = cardFooter + 
                    `
                    <button id= '${course._id}' value= 0 onclick="enableDisable('${course._id}', this.value)" id="disableButton" class="btn btn-danger text-white btn-block warningButton"> Disable Course </button> 
                    `

                }
            }
            	

			return (

                   `

                    <div class="col-md-6 my-3">
		                <div class='card'>
		                    <div class='card-body courseCss'>
		                        <h5 class='card-title'>${course.name}</h5>
		                            <p class='card-text text-left'>
		                                    ${course.description}
		                            </p>
		                            <p class='card-text text-right'>
		                                   ₱ ${course.price}
		                            </p>

		                            </div>
		                            <div class='card-footer'>
		                                ${cardFooter}
		                            </div>
		                        </div>
		                    </div>

                   `

				)

		}).join("") 

		let coursesContainer = document.querySelector("#coursesContainer");

		coursesContainer.innerHTML = courseData;

	}
})


let addButton = document.querySelector('#adminButton');

	if(adminUser == "false" || !adminUser){
	addButton.innerHTML = null;
	} else {
		addButton.innerHTML =
			`
     		<div class="col-md-2 offset-md-10 col-sm-4 ">
  			<a href="./addCourse.html" id="addCoursebtn" class="btn btn-block btn-info"> + Add Course </a>
  			</div>

			`

}

function enableDisable(courseId, isActive){
	//console.log(isActive);
    let button= document.getElementById(courseId);
    if(isActive == 1){
        // disable it
        fetch(`https://infinite-brook-06577.herokuapp.com/api/course/${courseId}`,{
            method: 'PUT',
                   headers: {
                   'Authorization': `Bearer ${token}`
                  }
        	
          })
        .then(res => res.json())
        .then(data => {
        	//console.log(data);
      
        })

        button.innerHTML = `Disable Course`;
        button.innerText= `Disable Course`;
        button.value = 0;
        alert('Successfully enabled');
        window.location.replace('./courses.html')
    } 

    else { // enable it
    	fetch(`https://infinite-brook-06577.herokuapp.com/api/course/${courseId}`,{
            method: 'DELETE',
                   headers: {
                   'Authorization': `Bearer ${token}`
                  }
        
          })
        .then(res => res.json())
        .then(data => {
        	//console.log(data);
          // do nothing
        })



        button.innerHTML = `Enable Course`;
        button.innerText = `Enable Course`;

        button.value = 1;
        alert('Successfully disabled');
        window.location.replace('./courses.html')
    }



}