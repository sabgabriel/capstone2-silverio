let profileContainer = document.querySelector("#profileContainer")
let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin")
let profile = document.querySelector('.jumbotron');


let myCourses = document.querySelector('#myCourses')

// let enrollments = localStorage.getItem("enrollments");
// console.log(enrollments);

let table = document.querySelector(".class");
let courseName = document.querySelector('#courseName');
let courseDate = document.querySelector('#courseDate');
let courseStatus = document.querySelector('#courseStatus')


//fetch loggedin user details

fetch (`https://infinite-brook-06577.herokuapp.com/api/users/details`, {
	headers: {
		"Content-Type": "application/json",
		"Authorization": `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {


		let courseEnrollments = document.querySelector("#enrollmentsContainer")

		data.enrollments.forEach(enrollments => {

			const list = document.querySelector('#course-list');
  			const row = document.createElement('tr');


			fetch (`https://infinite-brook-06577.herokuapp.com/api/course/${enrollments.courseId}`)
				.then(res => res.json())
				.then(data =>{
				//console.log(data);

				  courseName = data.name;

				  //Change time <td>${Date(enrollments.enrolledOn).toLocaleString()}</td>

				  row.innerHTML = 
				  `
				    <td>${courseName}</td>
				    <td>${new Date(enrollments.enrolledOn).toLocaleString()}</td>
				    <td>${enrollments.status}</td>
				  `

				  	list.appendChild(row);

				})

				  profile.innerHTML = 
					  `
						<h2 class='text-info'>WELCOME TO YOUR PROFILE</h2>
						<br>		  
					  		<h3>${data.lastName}, ${data.firstName}</h3>
							  Mobile number: ${data.mobileNo}
							  <br>
							  Email: ${data.email}

					  `	

				})

					//console.log(adminUser);

					if (adminUser == 'true') {
						// $(myCourses).remove();

						//remove table if Admin User

						myCourses.innerHTML = 	
						`
						<h5 class= 'text-danger text-center'>***ADMIN USER***</h5>

						`

						profile.innerHTML = 
						 `
							<h2 class='text-info'>ADMINISTRATOR</h2>
							<br>		  
						  		<h3>${data.lastName}, ${data.firstName}</h3>
								  Mobile number: ${data.mobileNo}
								  <br>
								  Email: ${data.email}


					 	 `	


					}

})
