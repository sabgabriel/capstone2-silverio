let params = new URLSearchParams(window.location.search); 
let courseId = params.get('courseId');
let token = localStorage.getItem('token');
let adminUser = localStorage.getItem("isAdmin");
console.log("courseId: " + courseId);

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrolleeContainer = document.querySelector("#enrolleeContainer")
let table = document.querySelector('#enrolleesTable')

fetch(`https://infinite-brook-06577.herokuapp.com/api/course/${courseId}`)
.then(res => res.json())
.then(data => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	if (adminUser == "false") {


fetch('https://infinite-brook-06577.herokuapp.com/api/users/details', {
        method: "GET",
        headers: {
            "Authorization": `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(userData => {



        userData.enrollments.map(coursesEnrolled => {


            let courseidsEnrollled = coursesEnrolled.courseId;



            if (courseidsEnrollled.includes(data._id)) {

                enrollContainer.innerHTML = `<h5 class="text-white card-footer">You are enrolled in this course.  </h5>`

            }



        });
        
// End get here
   
    })


// Show enroll button if not yet enrolled
		            
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

		document.querySelector("#enrollButton").addEventListener("click",()=>{

			fetch('https://infinite-brook-06577.herokuapp.com/api/users/enroll',{
				method:'POST',
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId

				})
			})
			.then(res => res.json())
			.then(data => {


				if(data){
					alert("You have enrolled successfully.")
					window.location.replace("./courses.html")
				} else {
					alert("Enrollment Failed.")
				}
			})
		})

	} 


	else if (adminUser == "true") { 


		if(data.enrollees.length === 0){
     				  
     		enrollContainer.innerHTML += `<h5 class="pt-5">There is no enrollee for this course yet.</h5>`

     	}
		
	
		data.enrollees.map(enrolleesInfo=>{

			fetch(`https://infinite-brook-06577.herokuapp.com/api/users/${enrolleesInfo.userId}`,{
					
					method: "GET",
					headers: {
			"Authorization": `Bearer ${token}` },
					
					
		})
				.then(res=>res.json())
				.then(data2 => {
					
					
				console.log(data2);


							const row = document.createElement('tr');

			        			row.innerHTML = 
			        			`
			        				<tr>
								       <td>${data2.firstName} ${data2.lastName}</td>
								       <td>${new Date(enrolleesInfo.enrolledOn).toLocaleString()}</td>
								    </tr>

			        			`
			        			table.appendChild(row);
						
					
				})
			
		}) 
	}
})
