//let params = new URLSearchParams(window.location.search)
let userId = localStorage.getItem('userId');
//console.log(userId)
let token = localStorage.getItem("token");

let firstName = document.querySelector("#firstName")
let lastName = document.querySelector("#lastName")
let mobileNo = document.querySelector("#mobileNumber")
let editProfile = document.querySelector("#editProfileButton")

  fetch('https://infinite-brook-06577.herokuapp.com/api/users/details', {
    headers: {
      'Authorization': `Bearer ${token}`
    }

  })
  .then(res => res.json())
  .then(data => {
  //console.log(data)

      firstName.placeholder = data.firstName
      lastName.placeholder = data.lastName
      mobileNo.placeholder = data.mobileNo

  let updateForm = document.querySelector("#editProfile")
  updateForm.addEventListener("submit", (e) => {
        e.preventDefault()


        let fName = firstName.value
        let lName = lastName.value
        let mNo = mobileNo.value

        console.log(fName)
        console.log(userId)

        fetch('https://infinite-brook-06577.herokuapp.com/api/users', {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          },
          body: JSON.stringify({
            id: userId,
            firstName: fName,
            lastName: lName,
            mobileNo: mNo
          })
        })
        .then(res => res.json())
        .then(data => {          console.log(data)
          if(data === true){
            alert("Profile was edited successfully")
            window.location.replace('./profile.html')
          }else{
            alert("Something went wrong.")
          }
        })
      })
})
