let token = localStorage.getItem("token")

//to convert the string value to boolean, add in === "true"
let adminUser = localStorage.getItem("isAdmin") === "true"

//render button if admin is user, else no button
//div id adminButton
let addButton = document.querySelector("#adminButton")

//This if statement will allow us to show a button for adding a course, a button to redirect to addCourse.html
//if isAdmin = false, they should not see a button

if (adminUser === false || adminUser === null) {
	addButton.innerHTML = null

} else {

	addButton.innerHTML = `

		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
		</div>

	`
}
		//get all courses
		fetch('https://infinite-brook-06577.herokuapp.com/api/course/')
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
