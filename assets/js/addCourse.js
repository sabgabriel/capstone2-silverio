let formSubmit = document.querySelector("#createCourse")

//add an eventListerner:
formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	//get the JWT from our localStorage

	let token = localStorage.getItem("token")
	console.log(token)


	//Create a fetch req to add a new course:
	fetch('https://infinite-brook-06577.herokuapp.com/api/course', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res => res.json())
	.then(data => {
		//if the creation of the course is successful, redirect admin to courses page.

		if (data === true) {
			window.location.replace('./courses.html')
		} else {
			//Error while creating a course:
			alert("Course creation failed. Something went wrong.")
		}
	})
})

